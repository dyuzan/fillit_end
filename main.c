/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/15 01:01:59 by dyuzan            #+#    #+#             */
/*   Updated: 2016/08/15 01:02:03 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_bzero(void *s, size_t n)
{
	unsigned char	*p;

	p = (unsigned char*)s;
	while (n > 0)
	{
		*p = 0;
		p++;
		n--;
	}
}

void	set_place(t_tetris *tab)
{
	int i;
	int j;

	i = 0;
	while (tab[i].shape != NULL)
	{
		j = 0;
		while (tab[i].shape[j] == '.')
			j++;
		tab[i].place = j;
		i++;
	}
}

int		nb_of_pieces(t_tetris *tetris)
{
	int i;

	i = 0;
	while (tetris[i].shape != NULL)
		i++;
	return (i);
}

int		main(int argc, char **argv)
{
	t_tetris	tab[27];
	t_result	res;

	ft_bzero(tab, 27 * sizeof(*tab));
	fill_with_points(&res);
	if (argc == 2)
	{
		read_file(argv[1], tab);
		res.size = ft_sqrt(nb_of_pieces(tab) * 4);
		set_place(tab);
		ft_solve_tetris(&res, tab, 0);
	}
	exit_error(20);
}
