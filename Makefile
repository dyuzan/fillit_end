
NAME = fillit

SRCS = main.c			\
	   aux_functions.c	\
	   solve_tetris.c	\
	   validation.c 	\
	   text_file.c 		\
	   result.c			\

CFLAGS = -Wall -Wextra -Werror

OBJS = $(SRCS:.c=.o)

$(NAME)	:	$(OBJS)
			gcc $(CFLAGS) -o $(NAME) $(OBJS)

all		:	$(NAME)

clean	:
			rm -rf $(OBJS)

fclean	:	clean
			rm -rf $(NAME)

re		:	fclean all